from sqlalchemy import Table, create_engine, MetaData, Column, String, Integer, ForeignKey
import locale


def connect(host, port, user, pw, schema):
    url = 'postgresql://{}:{}@{}:{}/{}'
    url = url.format(user, pw, host, port, schema)

    con = create_engine(url, client_encoding='utf8')

    meta = MetaData(bind=con, reflect=True)

    return con, meta


def init_staging(con, meta):
    """
    Reset and init the DB.
    There could be an ORM approach but in this task it is not necessary.

    :param con:
    :param meta:
    :param countries:
    :param migration:
    :param remittance:
    :return:
    """

    meta.clear()
    meta.reflect()
    meta.drop_all()
    meta.clear()

    country_tbl = Table(
        'country',
        meta,
        Column('country_id', Integer, primary_key=True),
        Column('name', String)
    )

    migration_tbl = Table(
        'migration',
        meta,
        Column('from_country_id', ForeignKey('country.country_id')),
        Column('to_country_id', ForeignKey('country.country_id')),
        Column('amount', Integer)
    )

    remittance_tbl = Table(
        'remittance',
        meta,
        Column('from_country_id', ForeignKey('country.country_id')),
        Column('to_country_id', ForeignKey('country.country_id')),
        Column('amount', Integer)
    )

    meta.create_all()


def populate_staging(con, meta, countries, migration, remittance):
    """
    Populate tables with the data extracted from CSV.
    This means transform the non-relational data to relational by mapping the countries correctly. (such as Other North?) :)
    :param con:
    :param meta:
    :param countries:
    :param migration:
    :param remittance:
    :return:
    """
    country_tbl = meta.tables['country']
    migration_tbl = meta.tables['migration']
    remittance_tbl = meta.tables['remittance']

    con.execute(country_tbl.insert(), [{'name': x} for x in countries])

    # Instead of individual subselects let's cache the country ids and flip them around for easy lookup
    inserted_countries = con.execute(country_tbl.select()).fetchall()
    country_dict = {name: country_id for country_id, name in inserted_countries}

    # Flatten migration and insert
    print('Processing migration...')
    flattened_migration = []
    for from_country_name, mig_row in migration.items():
        flattened_migration.extend(
            [
                {
                    'from_country_id': country_dict[from_country_name],
                    'to_country_id': country_dict[to_country_name],
                    'amount': locale.atoi(amount)
                }
                for to_country_name, amount in mig_row.items()
            ]
        )

    con.execute(
        migration_tbl.insert(),
        flattened_migration
    )

    # This might look like repetition... but the 2 data structures are not necessarily identical.
    print('Processing remittance...')
    flattened_remittance = []
    for from_country_name, rem_row in remittance.items():
        flattened_remittance.extend(
            [
                {
                    'from_country_id': country_dict[from_country_name],
                    'to_country_id': country_dict[to_country_name],
                    'amount': locale.atoi(amount)
                }
                for to_country_name, amount in rem_row.items()
            ]
        )

        con.execute(
            remittance_tbl.insert(),
            flattened_remittance
        )
