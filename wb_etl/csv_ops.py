import csv


def read_seed_data(file_name):
    """
    This function is returning a 2 dimensional data table extracted from the provided csv file
    :param file_name:
    :return:
    """

    data_table = {}
    with open(file_name, 'r') as infile:
        reader = csv.DictReader(infile)
        for row in reader:
            sender = row.pop('Sender')
            data_table[sender] = row
    return data_table
