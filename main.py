
import locale
from wb_etl import db, csv_ops

DB_HOST = 'localhost'
DB_PORT = 5432
DB_USER = 'pguser'
DB_PASS = 'pgpass'
STAGING_DB = 'staging'
DWH_DB = 'dwh'
MIGRATION_CSV = 'bilateralmigrationmatrix20130.csv'
REMITTANCE_CSV = 'bilateralremittancematrix2016_Nov2017.csv'


def main():
    # The dataset uses thousand separators(commas in my Excel), which is in specific locale
    # For linux/osx the locale is most likely 'en_US.UTF-8'
    # Alternatively one can run replace on the strings as well.
    locale.setlocale(locale.LC_ALL, 'english_USA')  # This is the Windows alternative

    print('Starting...')
    print('Reading CSV files')
    migration_data = csv_ops.read_seed_data(MIGRATION_CSV)
    remittance_data = csv_ops.read_seed_data(REMITTANCE_CSV)
    countries = set(migration_data.keys()).union(set(remittance_data.keys()))

    print('Set up staging db')
    staging_con, staging_meta = db.connect(DB_HOST, DB_PORT, DB_USER, DB_PASS, STAGING_DB)
    print('Reset, init schema')
    db.init_staging(staging_con, staging_meta)
    print('Populate staging')
    db.populate_staging(staging_con, staging_meta, countries, migration_data, remittance_data)


if __name__ == '__main__':
    main()
